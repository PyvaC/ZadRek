/**
 * Created by lukas on 19.07.2017.
 */
$(document).ready(function () {
    $('#submit').on('click', function(){
        var v1 = checkName();
        var v2 = checkEmail();
        var v3 = checkPesel();
        var v4 = checkQuantity();

        if(v1 && v2 && v3 && v4){
            sendValues();
        }
    });

    function sendValues(){
        var name = $('#name').val();
        var email = $('#email').val();
        var pesel = $('#pesel').val();
        var quantity = $('#quantity').val();

        $.post(
            'send',
            {
                nameVal: name,
                emailVal: email,
                peselVal: pesel,
                quantityVal: quantity
            },
            function(data, status){
                var obj = jQuery.parseJSON(data);
                if(obj.txt == "ok"){
                    hideErr('#defaultErr');
                    hideErr('content');

                    var txt = "<p>Bilety zostały pomyślnie zakupione i wysłane drogą mailową</p>";
                    txt += "<p>Jeżeli chcesz dokupić kolejne kliknij na link:<br> <a href='/ZadRek/home'>Home</a></p>";
                    showErr('.content', txt);
                }else{
                    showErr('#defaultErr', obj.txt);
                }
            }
        );
    }

    function checkName(){
        var name = $('#name').val();
        var regex = /^[a-zA-ZęóśłżźćńĘÓŚŁŻŹŃ]+\40?\-?[a-zA-ZęóśłżźćńĘÓŚŁŻŹŃ]+$/;

        if(regex.test(name)){
            hideErr('#nameErr');
            return true;
        }else{
            showErr('#nameErr', 'Podane imię jest nieprawidłowe.')
            return false;
        }

    }

    function checkEmail(){
        var email = $('#email').val();
        var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if(regex.test(email)){
            hideErr('#emailErr');
            return true;
        }else{
            showErr('#emailErr', 'Podany adres e-mail jest nieprawidłowy.')
            return false;
        }
    }

    function checkPesel(){
        var pesel =  $('#pesel').val();
        var regex = /^[0-9]{11}$/

        if(regex.test(pesel)){
            var year = parseInt(pesel.substring(0,2),10);
            var month = parseInt(pesel.substring(2,4),10);
            var day = parseInt(pesel.substring(4,6),10);

            if(month>80){
                showErr('#peselErr', 'Podany pesel wskazuje na osobę urodzoną w roku 18'+year+".");
                return false;
            }else if(month > 60 || month > 40){
                showErr('#peselErr', 'Podany pesel wskazuje na osobę która się jeszcze nie urodziła.');
                return false;
            }else if(month > 20){
                year = year + 2000;
                if(year>2017){
                    showErr('#peselErr', 'Podany pesel wskazuje na osobę która się jeszcze nie urodziła.');
                    return false;
                }
            }

            var weights = [9,7,3,1,9,7,3,1,9,7];
            var sum = 0;

            for(var i = 0; i<weights.length; i++){
                sum+=(parseInt(pesel.substring(i,i+1),10) * weights[i]);
            }
            sum=sum%10;
            var valid = (sum===parseInt(pesel.substring(10,11),10));

            if(valid){
                hideErr('#peselErr');
                return true;
            }else{
                showErr('#peselErr', 'Podany pesel jest niepoprawny.');
                return false;
            }

        }else{
            showErr('#peselErr', 'Podany pesel jest niepoprawny.')
            return false;
        }
    }

    function checkQuantity(){
        var quantity = $('#quantity').val();
        var regex = /^[1-3]$/;

        if(regex.test(quantity)){
            hideErr('#quantityErr');
            return true;
        }else{
            showErr('#quantityErr', 'Możliwe jest zakupienie maksymalnie 3 biletów przez jedną osobę.')
            return false;
        }
    }

    function showErr(id, txt){
        $(id).html(txt);
        $(id).show('slow');
    }

    function hideErr(id){
        $(id).hide('slow',function(){
            $(id).html("");
        });

    }
});
