CREATE DATABASE bilety
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_general_ci;

CREATE USER 'biletyUser'@'localhost' IDENTIFIED BY 'password';

GRANT ALL PRIVILEGES ON bilety . * TO 'biletyUser'@'localhost';

USE bilety;

CREATE TABLE kupione(
  id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  imie VARCHAR(256) NOT NULL,
  email VARCHAR(256) NOT NULL,
  pesel VARCHAR(11) NOT NULL,
  ilosc INT NOT NULL
)



