<?php
/*
 * Created by Lukasz Wypchal
 */
require_once('./includes.php');

$route = new Route();

$route->add('/', function(){

    $connection = new Connection();
    $sum = $connection->getQuantityOfTickets();

    $hc = new HtmlCreator();

    if($sum < 10){
        $hc->displayAll(function(){
            HtmlCreator::createForm();
        });
    }else{
        $hc->displayAll(function(){
            HtmlCreator::blockedForm();
        });
    }


});

$route->add('/home', function(){
    $connection = new Connection();
    $sum = $connection->getQuantityOfTickets();

    $hc = new HtmlCreator();

    if($sum < 10){
        $hc->displayAll(function(){
            HtmlCreator::createForm();
        });
    }else{
        $hc->displayAll(function(){
            HtmlCreator::blockedForm();
        });
    }


});

$route->add('/panel-administracyjny', function(){
    $connection = new Connection();
    $tickets = $connection->getTickets();

    $hc = new HtmlCreator();
    $hc->displayPanel($tickets);

});

$route->add('/send', function(){

    $name = $_POST['nameVal'];
    $email = $_POST['emailVal'];
    $pesel = $_POST['peselVal'];
    $quantity = $_POST['quantityVal'];


    $ticket = new TicketModel($name, $email, $pesel, $quantity);
    $ticketController = new TicketController($ticket);
    $txt = $ticketController->buyTickets();

    $tmp = array('txt'=>$txt);
    echo json_encode($tmp);
});

$route->submit();


