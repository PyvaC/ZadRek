<?php
require_once('./includes.php');

class TicketController
{
    private $_ticket;
    private $_connection;

    public function __construct($ticket)
    {
        $this->_ticket = $ticket;
        $this->_connection = new Connection();
    }

    public function buyTickets(){

        $txt = $this->_ticket->validateTicket();

        if($txt != "ok"){
            return $txt;
        }

        $txt = $this->checkPossibilityOfUser();

        if($txt != "ok"){
            return $txt;
        }

        $txt = $this->checkPossibility();

        if($txt != "ok"){
            return $txt;
        }

        $this->_connection->setTicket($this->_ticket);

        return $txt;

    }

    public function checkPossibility(){
        $sum = $this->_connection->getQuantityOfTickets();
        $txt = "";
        if($sum >= 10){
            $txt = "Wszystkie bilety zostały już wyprzedane.";
        }else{
            if($sum + $this->_ticket->getQuantity() <= 10){
                $txt = "ok";
            }else{
                $possibleQuantity = 10 - $sum;
                $txt = "Został(y) tylko " . $possibleQuantity . " bilet(y) możliwy(e) do kupienia";
            }
        }

        return $txt;
    }

    public function checkPossibilityOfUser(){
        $sum = $this->_connection->getQuantityOfTicketsForPerson($this->_ticket->getPesel());
        $txt = "";

        if($sum >=3){
            $txt="Nie możesz już kupić żadnego biletu (max ilość: 3)";
        }else{
            if(3-$sum >= $this->_ticket->getQuantity()){
                $txt = "ok";
            }else{
                $txt = "Kupiłeś już wcześniej " . $sum . " bilet(y) (max ilośc: 3).";
            }
        }

        return $txt;
    }


}