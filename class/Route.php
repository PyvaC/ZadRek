<?php

class Route
{
    private $_listOfUri = array();
    private $_listOfFunctions = array();

    private $_forTrim = '/\^$';

    public function add($uri, $function){
        $uri = trim($uri, $this->_forTrim);

        array_push($this->_listOfUri, $uri);
        array_push($this->_listOfFunctions, $function);
    }

    public function submit(){
        $uri = isset($_REQUEST['uri']) ? $_REQUEST['uri'] : '/';

        $uri = trim($uri, $this->_forTrim);

        for($i = 0; $i < count($this->_listOfUri); $i++){
            if(preg_match("#^$uri$#", $this->_listOfUri[$i])){
                call_user_func($this->_listOfFunctions[$i]);
            }
        }
    }
}