<?php

class Connection{

    private $_pdo;

    public function __construct(){
        $this->_pdo = $this->connect();
    }

    public function connect(){
        try{
            $pdo = new PDO('mysql:host=localhost;dbname=bilety;charset=utf8', 'biletyUser', 'password');

            return $pdo;
        }catch (PDOException $e){
            echo "Error: " . $e->getMessage() . "<br>";
            die();
        }
    }

    public function setTicket($ticket){
        $stmt = "INSERT INTO kupione (imie, email, pesel, ilosc) VALUES (:imie, :email, :pesel, :ilosc)";
        $q = $this->_pdo->prepare($stmt);
        $name = $ticket->getName();
        $email = $ticket->getEmail();
        $pesel = $ticket->getPesel();
        $quantity = $ticket->getQuantity();
        $q->bindParam(":imie", $name);
        $q->bindParam(":email", $email);
        $q->bindParam(":pesel", $pesel);
        $q->bindParam(":ilosc", $quantity);
        $q->execute();
    }

    public function getTickets(){
        $stmt = "SELECT imie, email, pesel, sum(ilosc) as ilosc FROM kupione group by pesel";
        $q = $this->_pdo->prepare($stmt);
        $q->execute();

        $result = $q->fetchAll();

        $tickets = array();

        foreach($result as $row){
            $ticket = new TicketModel($row['imie'], $row['email'], $row['pesel'], $row['ilosc']);
            array_push($tickets, $ticket);
        }

        return $tickets;
    }

    public function getQuantityOfTickets(){

        $stmt = "SELECT ilosc FROM kupione";
        $q = $this->_pdo->prepare($stmt);
        $q->execute();

        $result = $q->fetchAll();
        $sum = 0;
        foreach ($result as $row){
            $sum+= (int)$row['ilosc'];
        }

        return $sum;
    }

    public function getQuantityOfTicketsForPerson($pesel){
        $stmt = "SELECT ilosc FROM kupione WHERE pesel=:pesel";
        $q = $this->_pdo->prepare($stmt);
        $q->bindParam(':pesel', $pesel);
        $q->execute();

        $result = $q->fetchAll();
        $sum = 0;
        foreach ($result as $row){
            $sum+= (int)$row['ilosc'];
        }

        return $sum;
    }

    /**
     * @return mixed
     */
    public function getPdo()
    {
        return $this->_pdo;
    }

    /**
     * @param mixed $pdo
     */
    public function setPdo($pdo)
    {
        $this->_pdo = $pdo;
    }
}