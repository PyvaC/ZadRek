<?php
require_once('./includes.php');
class HtmlCreator
{
    public function displayAll($function = null){
        $this->createHeader();

        if($function != null){
            call_user_func($function);
        }

        $this->createFooter();
    }

    public static function blockedForm(){
        ?>
        <div class="container">
            <div class="row">
                <p>Wszytskie bilety zostały już sprzedane,<br> możliwość zakupu została zablokowana. </p>
            </div>
        </div>
        <?php
    }

    public static function createForm(){
        ?>
            <div class="container">
                <div class="row">
                    <div class="content">
                        <form class="form" method="post" action="../index.php" accept-charset="UTF-8">

                            <div class="form-group">
                                <label for="name">Imię:</label>
                                <input type="text" class="form-control" id="name">
                            </div>

                            <div id="nameErr"></div>

                            <div class="form-group">
                                <label for="email">E-mail:</label>
                                <input type="email" class="form-control" id="email">
                            </div>

                            <div id="emailErr"></div>

                            <div class="form-group">
                                <label for="pesel">PESEL:</label>
                                <input type="text" class="form-control" id="pesel">
                            </div>

                            <div id="peselErr"></div>

                            <div class="form-group">
                                <label for="quantity">Ilość biletów:</label>
                                <input type="text" class="form-control" id="quantity">
                            </div>

                            <div id="quantityErr"></div>

                            <button id="submit" type="button" class="btn btn-primary">Zamów</button>
                        </form>
                            <div id="defaultErr"></div>
                    </div>
                </div>
            </div>
        <?php
    }

    private function createHeader()
    {
        ?>
        <html>
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <title>Zadanie Rekrutacyjne</title>
            <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
                  integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
                  crossorigin="anonymous">
            <link href="./assets/style.css" rel="stylesheet">
        </head>
        <body>
        <nav class="nav">
            <div class="container">
                <div class="row">
                    <ul>
                        <?php
                        $cssHome ="";
                        $cssPanel ="";
                        if($_SERVER['REQUEST_URI'] == BASE_URL ||$_SERVER['REQUEST_URI'] == "home" ){
                            $cssHome = "clicked";
                        }else if($_SERVER['REQUEST_URI'] == BASE_URL."panel-administracyjny"){
                            $cssPanel = "clicked";
                        }
                        ?>
                        <li>
                            <a href="<?php echo BASE_URL; ?>home" class="<?php echo $cssHome; ?>">
                                Home
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo BASE_URL; ?>panel-administracyjny" class="<?php echo $cssPanel; ?>">
                                Panel administracyjny
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <?php
    }

    private function createFooter()
    {
        ?>
                <footer class="footer">
                    <div class="container">
                        <div class="row">
                            <span>Made by Łukasz Wypchał</span>
                        </div>
                    </div>
                </footer>

                <script src="https://code.jquery.com/jquery-3.2.1.min.js"
                        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
                        crossorigin="anonymous"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
                        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
                        crossorigin="anonymous"></script>
                <script src="./assets/form.js"></script>
            </body>
        </html>
        <?php
    }

    public function displayPanel($tickets){
        $this->createHeader();
        ?>
        <div class="container">
            <div class="row">

                <table class="table table-striped">
                    <thead>
                        <th>Id</th>
                        <th>Imię</th>
                        <th>E-mail</th>
                        <th>PESEL</th>
                        <th>Łączna liczba biletów</th>
                    </thead>
                    <tbody>
                    <?php
                    $txt ="";
                    $z = 1;
                    for($i = 0; $i < count($tickets); $i++){
                        $t = $tickets[$i];

                        $txt .= "<tr>";
                        $txt .= "<td>$z</td>";
                        $txt .= "<td>".$t->getName()."</td>";
                        $txt .= "<td>".$t->getEmail()."</td>";
                        $txt .= "<td>".$t->getPesel()."</td>";
                        $txt .= "<td>".$t->getQuantity()."</td>";

                        $txt .= "</tr>";
                        $z++;
                    }
                    echo $txt;
                    ?>

                    </tbody>
                </table>
            </div>
        </div>
        <?php
        $this->createFooter();
    }
}