<?php

class TicketModel
{
    private $_name;
    private $_email;
    private $_pesel;
    private $_quantity;

    public function __construct($name, $email, $pesel, $quantity)
    {
        $this->_name = $name;
        $this->_email = $email;
        $this->_pesel = $pesel;
        $this->_quantity = $quantity;
    }

    function validateName(){
        $regex = '/^.+\040?\-?.+$/';

        if(preg_match($regex, $this->getName())){
            return true;
        }else{
            return false;
        }
    }

    function validateEmail(){
        if(!filter_var($this->getEmail(), FILTER_VALIDATE_EMAIL)){
            return false;
        }else{
            return true;
        }
    }

    function validateQuantity(){
        $regex = '/^[1-3]$/';
        if(preg_match($regex, $this->getQuantity())){
            return true;
        }else{
            return false;
        }
    }

    function validatePesel(){
        $regex = '/^[0-9]{11}$/';
        $pesel = $this->getPesel();
        if(!preg_match($regex, $pesel)){
            return false;
        }

        $arrayWeights = array(1, 3, 7, 9, 1, 3, 7, 9, 1, 3);
        $sum = 0;

        for($i = 0; $i < count($arrayWeights); $i++){
            $sum += $arrayWeights[$i] * $pesel[$i];
        }

        $sum = 10 - $sum%10;
        $sum = ($sum == 10)?0:$sum;
        if($sum == $pesel[10]){
            return true;
        }

        return false;
    }

    public function validateTicket(){
        $v1 = $this->validateName();
        $v2 = $this->validateEmail();
        $v3 = $this->validatePesel();
        $v4 = $this->validateQuantity();

        if($v1 && $v2 && $v3 && $v4){
            return 'ok';
        }else{
            $txt = 'Jedno z pól zostało niepoprawnie wypełnione';
            return $txt;
        }
    }
    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->_email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->_email = $email;
    }

    /**
     * @return mixed
     */
    public function getPesel()
    {
        return $this->_pesel;
    }

    /**
     * @param mixed $pesel
     */
    public function setPesel($pesel)
    {
        $this->_pesel = $pesel;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->_quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity)
    {
        $this->_quantity = $quantity;
    }

}